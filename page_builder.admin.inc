<?php

function page_builder_admin_page($form, &$form_state, $module, $page = FALSE) {
  // dpm($page,'$page');
  // Attach info module.
  $form_state['page_builder']['module'] = $module;
  $form_state['page_builder']['page'] = $page;
  $form['#page_builder']['module'] = $module;
  if (!$page) {
    $form['page'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a new page'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
  }
  $form['page']['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#required' => TRUE,
    '#id' => 'asd',
    '#size' => 32,
    '#maxlength' => 64,
    '#description' => t('The machine readable name of this page. It must be unique, and it must contain only alphanumeric characters and underscores. Once created, you will not be able to change this value!'),
    '#machine_name' => array(
      'exists' => 'page_builder_check_name',
      'source' => array('page','options', 'title'),
      'label' => t('Machine name'),
    ),
  );
  if ($page) {
    $form['page']['name']['#type'] = 'textfield';
    $form['page']['name']['#value'] = $page->name;
    $form['page']['name']['#disabled'] = TRUE;
    $form['page']['name']['#description'] = t('The machine readable name of this page.');
  }
  $form['page']['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page options'),
    '#collapsible' => TRUE,
  );

  $form['page']['options']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 32,
    '#id' => 'asda',
    '#default_value' => $page ? $page->settings['title'] : '',
    '#maxlength' => 64,
    '#required' => TRUE,
    '#prefix' => theme('advanced_help_topic', array('module' => 'homebox', 'topic' => 'new-page')),
    '#description' => t('The title of the page that will be created.'),
  );

  $form['page']['options']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#required' => TRUE,
    '#size' => 32,
    '#default_value' => $page ? $page->settings['path'] : '',
    '#maxlength' => 255,
    '#description' => t('Apecify a URL by which this page can be accessed. For example, type "dashboard" when creating a Dashboard page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
  );
  $form['page']['options']['menu'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create a menu entry'),
    '#description' => t('If checked, a menu entry will be created for this page.'),
    '#default_value' => $page ? $page->settings['menu'] : 1,
  );
  $form['page']['options']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the page'),
    '#description' => t('If unchecked, only users with the <em>administer homebox</em> permission will be able to view this page.'),
    '#default_value' => $page ? $page->settings['enabled'] : 1,
  );

  $user_roles = user_roles();
  unset($user_roles[DRUPAL_ANONYMOUS_RID]);
  unset($user_roles[DRUPAL_AUTHENTICATED_RID]);
  $form['page']['options']['access'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow only certain roles to access the page'),
    '#default_value' => $page ? $page->settings['access'] : array(),
    '#options' => $user_roles,
    '#description' => t('Select which roles can view the page.'),
  );

  if ($page) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save page'),
      '#weight' => 2,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete page'),
      '#weight' => 3,
    );
  }
  else {
    $form['page']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add page'),
    );
    $form['#validate'][] = 'page_builder_admin_page_validate';
    $form['#submit'][] = 'page_builder_admin_page_submit';
  }

  return $form;
}

function page_builder_check_name($name) {
  return db_query('SELECT 1 FROM {page_builder_pages} WHERE name = :name', array(':name' => $name))->fetchField();
}

function page_builder_check_path($path) {
  return db_query("SELECT * FROM {menu_router} where path = :path", array(':path' => $path))->fetchAssoc();
}

function page_builder_admin_page_validate(&$form, &$form_state) {
  // debug_ijor($form_state,'$form_state', 'f:__12');
  // No valifation needed on Delete.
  if ($form_state['values']['op'] == t('Delete page')) {
    return;
  }
  // Validate title.
  $test = strip_tags($form_state['values']['title']);
  if ($test != $form_state['values']['title']) {
    form_set_error('title', t('Limit the title to non HTML characters.'));
  }
  // Make sure a title is present.
  if (empty($form_state['values']['title'])) {
    form_set_error('title', t('You must enter a page title.'));
  }
  $path = $form_state['values']['path'];
  $old_path = !empty($form_state['page_builder']['page']) ? $form_state['page_builder']['page']->settings['path'] : NULL;
  
  // Check if the path exists.
  if (isset($old_path) && $old_path != $path && page_builder_check_path($path)) {  
    // Jika ini merupakan halaman edit, maka cek path hanya jika 
    // berbeda dengan sebelumnya.
    form_set_error('path', t('A path is exists.'));
  }
  if (!isset($old_path) && page_builder_check_path($path)) {
    form_set_error('path', t('A path is exists.'));
  }
  // Ensure path fits the rules.  
  if (preg_match('/[^-a-z0-9_\/]/', $path)) {
    form_set_error('path', t('Path must be lowercase alphanumeric, underscores, dashes, or forward-slashes only.'));
  }
  // Check path for preceeding or trailing forward slashes.
  if (substr($path, 0, 1) == '/' || substr($path, strlen($path) - 1, 1) == '/') {
    form_set_error('path', t('Path cannot begin or end with a slash.'));
  }
}

function page_builder_admin_page_submit(&$form, &$form_state) {  
  $op = $form_state['values']['op'];
  $module = $form_state['page_builder']['module'];
  if ($op == t('Delete page')) {
    // Redirect to the confirmation message.
    // Reverting and deleting is the same operation.
    $form_state['redirect'] = 'admin/structure/page_builder/' . $module . '/delete/' . $form_state['values']['name'];
    return;
  }
  elseif ($op == t('Add page')) {
    $page = new stdClass;
    $page->name = $form_state['values']['name'];
    $page->module = $module;
    page_builder_admin_form_to_page($page, $form_state);
    drupal_set_message(t('The page has been added.'));
    page_builder_save_page($page);
    $menu_rebuild = TRUE;
  }
  elseif ($op == t('Save page')) {
    // Fetch old page to amend.    
    $page = page_builder_get_page($form_state['values']['name']);
    // 
    $is_title_changed = ($form_state['values']['title'] != $page->settings['title']);
    $is_path_changed = ($form_state['values']['path'] != $page->settings['path']);
    $is_menu_changed = ($form_state['values']['menu'] != $page->settings['menu']);
    $is_enabled_changed = ($form_state['values']['enabled'] != $page->settings['enabled']);
    $is_access_changed = (array_keys(array_filter($form_state['values']['access'])) != $page->settings['access']);
    if ($is_title_changed || $is_path_changed || $is_menu_changed || $is_enabled_changed || $is_access_changed) {
      $menu_rebuild = TRUE;
      page_builder_admin_form_to_page($page, $form_state);
      drupal_set_message(t('Changes have been saved.'));
      page_builder_save_page($page);
    }
  }
  
  $form_state['redirect'] = 'admin/structure/page_builder/' . $module;
  if (isset($menu_rebuild) && $menu_rebuild) {
    drupal_set_message(t('Menu has been rebuild.'));
    menu_rebuild();
  }
}

function theme_page_builder_admin_new_page($variables) {
  // debug_ijor($variables,'$form', 'f:__1');
  $form = $variables['form'];
  $module = $form['#page_builder']['module'];
  // debug_ijor($module,'$module', 'f:__1');
  $output = drupal_render_children($form);
  $data = module_invoke($module, 'page_builder');
  // debug_ijor($data,'$data', 'f:__1');
  $pages = page_builder_pages($module);
  // debug_ijor($pages,'$pages', 'f:__1');
  // return 'aku';
  $header = array(t('Name'), t('Operations'));
  if (is_array($pages) && !empty($pages)) {
    foreach ($pages as $page) {
      $cols = array();
      $cols[] = l($page->settings['title'], $page->settings['path']);
      // dpm($page,'$page');
      $links = array();
      if (!empty($data['settings callback'])) {
        $links[] = array('title' => t('Settings'), 'href' => 'admin/structure/page_builder/' . $module . '/settings/' . $page->name);
      }
      if (!empty($data['layout callback'])) {
        $links[] = array('title' => t('Layout'), 'href' => 'admin/structure/page_builder/' . $module . '/layout/' . $page->name);
      }
      $links[] = array('title' => t('Edit'), 'href' => 'admin/structure/page_builder/' . $module . '/edit/' . $page->name);
      $links[] = array('title' => t('Clone'), 'href' => 'admin/structure/page_builder/' . $module . '/clone/' . $page->name);      
      $element_operations = array(
        '#theme' => array('links__ctools_dropbutton', 'links'),
        '#links' => $links,
      );
      $cols[] = drupal_render($element_operations);
      $rows[] = $cols;
    }
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No content available.')));
    $css = '
    .ctools-button-processed {
      background-color: #ffffff;
      border-color: #cccccc;
      font-size: 11px;
      padding-bottom: 2px;
      padding-top: 2px;
    }
    .ctools-button-processed {
      background-image: linear-gradient(-90deg, #ffffff 0px, #f9f9f9 100%);
      border-radius: 11px;
    }
    .ctools-dropbutton-processed .ctools-content {
      border-right: 1px solid #e8e8e8;
    }
    .ctools-button-processed .ctools-content {
      padding-bottom: 0;
      padding-top: 0;
    }';
    drupal_add_css($css, array('type' => 'inline', 'group' => CSS_THEME));
  }
  return $output;
}

function page_builder_clone_page($form, &$form_state, $module, $page = FALSE) {
  $form = page_builder_admin_page($form, $form_state, $module, $page);    
  $form['page']['name']['#type'] = 'machine_name';
  unset($form['page']['name']['#disabled']);
  unset($form['page']['name']['#value']);
  unset($form['delete']);
  
  $form['import']['#type'] = 'value';
  $form['import']['#value'] = $page;
  
  $form['submit']['#value'] = t('Add page');
  $form['#validate'][] = 'page_builder_admin_page_validate';
  $form['#submit'][] = 'page_builder_clone_page_submit';
  // dpm($form,'$form');
  return $form;
}

function page_builder_clone_page_submit($form, &$form_state) {
  $page = new stdClass();
  $page->name = $form_state['values']['name'];
  if ($form_state['values']['import']) {
    // First get the settings from the cloned homebox.
    $page->module = $form_state['values']['import']->module;
    $page->settings = $form_state['values']['import']->settings;
    // Now overwrite with the values from the form.
    page_builder_admin_form_to_page($page, $form_state);
  }
  page_builder_save_page($page);
  drupal_set_message(t('The page has been added.'));
  $form_state['redirect'] = 'admin/structure/page_builder/' . $page->module;
  menu_rebuild();
}

function page_builder_delete_confirm_page($form, &$form_state, $module, $page) {
  // $form['#operation'] = homebox_page_is_api($page->name) ? t('revert') : t('delete');
  $form['#operation'] = t('delete');
  $form_state['page_builder']['page'] = $page;
  $form_state['page_builder']['module'] = $module;  
  $description = '<p>' . t('This action cannot be undone.') . '</p>';
  $question = t('Are you sure you want to !operation page %title?', array('!operation' => $form['#operation'], '%title' => $page->settings['title']));
  $path = 'admin/structure/page_builder/' . $module;
  return confirm_form($form, $question, $path, $description, ucfirst($form['#operation']));
}

function page_builder_delete_confirm_page_submit($form, &$form_state) {
  // Delete the page.
  page_builder_delete_page($form_state['page_builder']['page']->name);  
  drupal_set_message(t('The page has been deleted.'));
  $form_state['redirect'] = 'admin/structure/page_builder/' . $form_state['page_builder']['module'];
  menu_rebuild();
}

function page_builder_admin_form_to_page(&$page, $form_state) {
  $page->settings['title'] = $form_state['values']['title'];
  $page->settings['path'] = $form_state['values']['path'];
  $page->settings['menu'] = (int) $form_state['values']['menu'];
  $page->settings['enabled'] = (int) $form_state['values']['enabled'];
  $page->settings['access'] = array_keys(array_filter($form_state['values']['access']));
}
